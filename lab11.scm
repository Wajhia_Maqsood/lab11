;AND
(define AND (lambda (m n) (lambda (a b) (n (m a b) b))))
;OR 
(define OR (lambda (m n) (lambda (a b) (n a(m a b) ))))
;add 
(define add (lambda (m n s z)(m s(n s z))))
;isZero
(define isZero(lambda (n)(n (lambda(x) (false)) true)))
;LT
(define LT(lambda (m n)(AND(NOT (EQ n m))(LEQ n m))))
;EQ
(define EQ(lambda (m n)(AND(IsZero ( m pred n ) )IsZero(n pred m))))
;Fixed point combinator
(define Y (lambda (x)(f x x)(f x x)))
;loop recursive function
(define loop (lambda (n)(EQ n 0)0 (add f(n-1)n)))
;if-else
(define F(lambda (m n)(OR(EQ m n)(LT n m))(f(Y F(n))(add m n))))
